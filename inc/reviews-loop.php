<?php 

 $args = array( 
 'orderby' => 'title',
 'post_type' => 'reviews'
 );
 
 $reviews_query = new WP_Query( $args );

?>
<div class="reviews-grid">
    <?php if ( $reviews_query->have_posts() ): while ( $reviews_query->have_posts() ) : $reviews_query->the_post(); ?>
    <div class="reviews-grid-item">
        <div class="reviews-grid">
            <div class="reviews-grid-item photo" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>

            <div class="reviews-grid-item text">
                <div class="reviews-grid-content">
                    <?php the_content(); ?>
                    <p><?php the_field('name'); ?><br><?php the_field('location'); ?></p>  
                </div>
            </div>
        </div>
    </div>
    <?php endwhile; else: ?> <p>Sorry, there are no posts to display</p> <?php endif; ?>
    <?php wp_reset_query(); ?>
</div>