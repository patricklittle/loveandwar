<?php  

// Create 1 Custom Post type for a Demo, called reviews
function create_post_type_reviews() {
    register_taxonomy_for_object_type('category', 'reviews'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'reviews');
    register_post_type('reviews', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Reviews', 'lww_reviews'), // Rename these to suit
            'singular_name' => __('Review', 'lww_reviews'),
            'add_new' => __('Add New', 'lww_reviews'),
            'add_new_item' => __('Add New Review', 'lww_reviews'),
            'edit' => __('Edit', 'lww_reviews'),
            'edit_item' => __('Edit Review', 'lww_reviews'),
            'new_item' => __('New Review', 'lww_reviews'),
            'view' => __('View Review', 'lww_reviews'),
            'view_item' => __('View Review', 'lww_reviews'),
            'search_items' => __('Search Reviews', 'lww_reviews'),
            'not_found' => __('No Reviews found', 'lww_reviews'),
            'not_found_in_trash' => __('No Reviews found in Trash', 'lww_reviews')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            //'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}