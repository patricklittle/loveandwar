<?php /* Template Name: Landing Page */ get_header(); ?>

	<main role="main" aria-label="Content">

		<section>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
            
            <div class="container">
                                    
                    <?php if( get_field('form_wysiwyg')): ?>

                        <div class="row">
            
                            <div class="column col-6">
                                
                                <div class="content-wrap">

                        			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        				<?php the_content(); ?>

                        				<?php comments_template( '', true ); // Remove if you don't want comments ?>

                        				<?php edit_post_link(); ?>

                        			</article>
                                
                                </div> 
                                
                            </div>
                            
                            <div class="column col-6">
                                
                                <div class="content-wrap">
                                    
                                    <?php the_field('form_wysiwyg'); ?>
                                </div>
                                
                            </div>
                            
                        </div>
                    <?php else: ?>
                        
                        <div class="content-wrap">
                        
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <?php the_content(); ?>

                                <?php comments_template( '', true ); // Remove if you don't want comments ?>

                                <?php edit_post_link(); ?>

                            </article>
                            
                        </div>
                        
                    <?php endif; ?>    
                
            </div>

		<?php endwhile; ?>

		<?php else: ?>

			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>

		<?php endif; ?>

		</section>

	</main>
	
	<?php get_template_part( 'inc/reviews-loop'); ?>

<?php get_footer(); ?>
