<?php get_header(); ?>

<div class="container">

	<main role="main" aria-label="Content" class="main">
			
			<section>
				
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
                
                <?php if(get_field('page_header_title')): ?>
                    <h1><?php the_field('page_header_title'); ?></h1>
                <?php endif; ?>
                
                <?php if(get_field('page_header_message','$post->ID')): ?>
                    <p><?php the_field('page_header_message'); ?></p>
                <?php endif; ?>

				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php the_content(); ?>

					<?php comments_template( '', true ); // Remove if you don't want comments ?>

					<br class="clearfix">

					<?php edit_post_link(); ?>

				</article>
				<!-- /article -->

			<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

				</article>
				<!-- /article -->

			<?php endif; ?>

			</section>
		
	</main>

	<?php // get_sidebar(); ?>

</div>

<?php get_footer(); ?>
