<?php get_header(); ?>

	<main role="main" aria-label="Content" class="main">
		<!-- section -->
		<section class="container">

			<h1><?php _e( 'Latest Posts', 'html5blank' ); ?></h1>

			<?php get_template_part('inc/loop'); ?>

			<?php get_template_part('inc/pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>
