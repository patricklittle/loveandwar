var gulp = require('gulp');
var watch = require('gulp-watch');
var rollup = require('rollup-stream');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var favicons = require('gulp-favicons');

// Javascript

gulp.task('javascript', function() {
    return rollup({
            entry: './src/js/scripts.js'
        })
        .on('error', err => {
            gutil.log('Javascript Error: ', gutil.colors.red(err.message))
        })
        // give the file the name you want to output with
        .pipe(source('scripts.js'))
        .pipe(buffer())
        .pipe(uglify())
        // and output to ./dist/app.js as normal.
        .pipe(gulp.dest('./assets/js/'));
});

// CSS Styles

gulp.task('sass', function() {
    gulp.src('src/sass/style.scss')
        // .pipe(watch('src/sass/*.scss'))
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .on('error', err => {
            gutil.log('SASS Error: ', gutil.colors.red(err))
        })
        .pipe(prefix('last 2 versions', '> 1%', 'ie 8', 'Android 2', 'Firefox ESR'))
        .pipe(gulp.dest('./'));
});

// Images

gulp.task('images', function() {
    return gulp.src('src/img/*')
        // .pipe(watch('src/images/*'))
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest('assets/images'));
});

// SVG

gulp.task('svg', function() {
    return gulp.src('src/svg/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [
          {removeViewBox: false},
          {cleanupIDs: false}
      ],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('assets/svg'));
});

// Favicons - Run it once and forget about it

gulp.task('favicon', function () {
    gulp.src('src/favicon/favicon.png')
        .pipe(favicons({
            // appName: 'My App',
            // appDescription: 'This is my application',
            // developerName: 'Hayden Bleasel',
            // developerURL: 'http://haydenbleasel.com/',
            background: '#020307',
            path: 'favicons/',
            //url: 'http://haydenbleasel.com/',
            display: 'standalone',
            orientation: 'portrait',
            version: 1.0,
            logging: false,
            online: false,
//            pipeHTML: true,
            replace: false,
            icons: {
                android: false,              // Create Android homescreen icon. `boolean`
                appleIcon: false,            // Create Apple touch icons. `boolean` or `{ offset: offsetInPercentage }`
                appleStartup: false,         // Create Apple startup images. `boolean`
                coast: false,      // Create Opera Coast icon with offset 25%. `boolean` or `{ offset: 25% }`
                favicons: true,             // Create regular favicons. `boolean`
                firefox: false,              // Create Firefox OS icons. `boolean` or `{ offset: offsetInPercentage }`
                windows: false,              // Create Windows 8 tile icons. `boolean`
                yandex: false                // Create Yandex browser icon. `boolean`
            }
        })).pipe(gulp.dest('assets/favicons'));
});

// Default Task

gulp.task('default', ['javascript', 'sass'], function () {
  gulp.watch('src/sass/**/*.scss', ['sass']);
  gulp.watch('src/js/**/*.js', ['javascript']);
  gulp.watch('src/images/*', ['images']);
  gulp.watch('src/svg/*', ['svg']);
//  gulp.watch('src/icons/*', ['icons']);
});
