    
			<!-- footer -->
			<footer class="footer clearfix" role="contentinfo">
                
                <div class="container">
                    
                    <div class="row">
                    
                        <div class="column col-4">
                            <h3>Stay Connected</h3>
                            <ul class="footer-links">
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Instagram</a></li>
                            </ul>
                        </div>
                        
                        <div class="column col-4">
                            <h3>Where to Listen</h3>
                            <ul class="footer-links">
                                <li><a href="#">iTunes</a></li>
                                <li><a href="#">Soundcloud</a></li>
                            </ul>
                        </div>
                        
                        <div class="column col-4">
                            <h3>Community</h3>
                            <ul class="footer-links">
                                <li><a href="#">The Gathering Network</a></li>
                            </ul>
                        </div>
                    </div>
                
                </div>
				
				<div class="container">
					<p class="copyright">
						&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. 
					</p>
				</div>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
