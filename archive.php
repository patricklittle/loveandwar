<?php get_header(); ?>

	<div class="container">
		<main role="main" aria-label="Content">
			<!-- section -->
			<section>

				<h1><?php _e( 'Archives', 'html5blank' ); ?></h1>

				<?php get_template_part('inc/loop'); ?>

				<?php get_template_part('inc/pagination'); ?>

			</section>
			<!-- /section -->
		</main>
	</div>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
