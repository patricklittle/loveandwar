<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400|Raleway:400,800" rel="stylesheet">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">
            
            <div class="navbar animated <?php if( get_field('include_page_header') || is_single() && get_the_post_thumbnail($post->ID)): echo 'has-featured-image'; endif; ?>">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo.svg" alt="Logo" class="logo-img">
                        </a>
                    </div>
                    <nav class="nav-header" role="navigation">
                        <div class="mobile-nav-toggle">
                            <div class="hamburger-icon">
                                <span></span>
                            </div>
                        </div>
                        <?php html5blank_nav(); ?>
                    </nav>
                </div>
            </div>

			<!-- header -->
			<?php if( get_field('include_page_header') || is_single() && get_the_post_thumbnail($post->ID)): ?>
				<header class="header header-with-feature clearfix" role="banner">
					
					<?php 
						// Get Custom Fields
						$post =  $post->ID;
						$pageHeaderTitle = get_field('page_header_title', $post);
						$pageHeaderMessage = get_field('page_header_message', $post);
						$pageHeaderImage = get_field('page_header_image', $post);
					?>
					
					<div class="header-message">
	                    
	                    <div class="container">
								                        
	                        <?php 
		                        if($pageHeaderTitle) { echo '<h1>'. $pageHeaderTitle .'</h1>'; };
		                        if($pageHeaderMessage) { echo $pageHeaderMessage; };
	                        ?>
                            
                            <?php if(is_single() && !$pageHeaderTitle): ?>
                                <h1><?php the_title(); ?></h1>
                                <span class="date">
                                    <?php echo date('F j, Y', strtotime($post->post_date)); 
                                    
                                    echo $post;
                                    ?>
                    			</span>
                            <?php endif; ?>

    	                    </div>
	                    
					</div>
                    
                    <?php if(is_front_page()) :  // Show the video ?>
                        <video playsinline autoplay muted loop poster="<?php echo $pageHeaderImage; ?>" id="bgvid" preload>
							<!-- <source src="<?php //echo get_bloginfo('template_url') ?>/assets/video/mean-mule-loop-low.webm" type="video/webm" > -->
                            <source src="<?php echo get_bloginfo('template_url') ?>/assets/video/love-and-war-loop.mp4" type="video/mp4" >
                            <source src="<?php echo get_bloginfo('template_url') ?>/assets/video/mean-mule-loop-low.ogv" type="video/ogg" >
                            <object data="<?php echo get_bloginfo('template_url') ?>/assets/video/love-and-war-loop.mp4" width="1920" height="1080">
                                <param name="wmode" value="transparent">
                                <param name="autoplay" value="true" >
                                <param name="loop" value="true" >
                            </object>
                        </video>
                    <?php else: ?>
                        
                        <?php if($pageHeaderImage): ?> 
                            <div class="cover-image" style="background-image:url(<?php echo $pageHeaderImage; ?>)"></div>
                        <?php endif; ?>
                        
                        <?php if(get_the_post_thumbnail()): ?>
                            <div class="cover-image" style="background-image:url(<?php the_post_thumbnail_url('full'); ?>)"></div>
                        <?php endif; ?>
                        
                    <?php endif; ?>
				</header>
			
			<?php else: ?>
				<header class="header clearfix" role="banner">
					<div class="container">
	                    <div class="header-message">
	                        <h1><?php the_title(); ?></h1>                
	                    </div>
					</div>
				</header>
			<?php endif; ?>
			<!-- /header -->
