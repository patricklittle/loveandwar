
(function( root, $, undefined ) {
	"use strict";

	$(function () {
        // DOM ready, take it away
        
        // navbar fade in and out 
        $(window).scroll(function(){
            var scrollTop = $(window).scrollTop();

            if (scrollTop > 100) {
                $('.navbar').addClass('on-scroll');
            } else {
                $('.navbar').removeClass('on-scroll');
            }
                        
        });
		
	});

} ( this, jQuery ));